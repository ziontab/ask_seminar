from django.shortcuts import render, redirect
from askapp.models import Article, Profile
from askapp.forms import ProfileForm, AuthorForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.conf import settings

from django.views.decorators.cache import cache_page

import requests
import random

def index(request):
    return render(request, 'askapp/index.html', {
        'articles' : Article.objects.is_published()
    })

@login_required
def test(request):
    if request.user.is_authenticated():
        return HttpResponse("Auth ok: {}".format(request.user.username))
    else:
        return HttpResponse("Auth failed")

def signup(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            email    = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            address  = form.cleaned_data.get('address')
            user = User.objects.create_user(username, email, password)
            Profile.objects.create(user=user, address=address)
    else:
        form = ProfileForm()
    return render(request, 'askapp/signup.html', {
        'form' : form
    })

def author(request):
    if request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = AuthorForm()
    return render(request, 'askapp/author.html', {
        'form' : form
    })

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(username, password)
        if user:
            login(request, user)
            redirect(settings.LOGIN_REDIRECT_URL)

    return render(request, 'askapp/login.html', {})

# @cache_page(10)
def push(request):
    rand = random.randint(0, 100)
    requests.post('http://127.0.0.1:7777/pub',data={'msg':rand})
    return HttpResponse(rand)
