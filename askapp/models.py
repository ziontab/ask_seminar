# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Author(models.Model):
    name     = models.CharField(max_length=255, verbose_name = u"Имя")
    birthday = models.DateField(verbose_name = u"День рождения")

    class Meta:
        verbose_name = u"Автор"
        verbose_name_plural = u"Авторы"

    def calc(self):
        pass

    def __unicode__(self):
        return self.name

class ArticleManager(models.Manager):
    def is_published(self):
        return self.filter(is_published=True)

class Article(models.Model):
    title = models.CharField(max_length=255, verbose_name = u"Название")
    text  = models.TextField(verbose_name = u"Текст")
    is_published = models.BooleanField(default=False, verbose_name = u"Опубликовано")

    authors = models.ManyToManyField(Author, verbose_name=u'Авторы')

    objects = ArticleManager()

    class Meta:
        verbose_name = u"Статья"
        verbose_name_plural = u"Статьи"

    def __unicode__(self):
        return self.title

class Profile(models.Model):
    address = models.TextField(verbose_name='Адрес')
    user    = models.OneToOneField(User)

    class Meta:
        verbose_name = u"Профиль"
        verbose_name_plural = u"Профили"

    def __unicode__(self):
        return self.user.username
