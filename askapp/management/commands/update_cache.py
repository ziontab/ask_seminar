# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.core.cache import caches
import logging
import random

class Command(BaseCommand):
    help = 'updates cache'

    def handle(self, *args, **options):
        cache = caches['default']
        logging.warning('Existing value {}'.format(cache.get('test_key')))
        cache.set('test_key', random.randint(0, 1000))
        logging.warning('Existing value {}'.format(cache.get('test_key')))
        return None
