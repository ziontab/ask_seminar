from django import forms
from askapp.models import Author

class ProfileForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={"class":"testclass2"}))
    email    = forms.EmailField()
    password = forms.CharField()
    address  = forms.CharField()

class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name', 'birthday']
